//
//  TicTacToePlayerTests.swift
//  TicTacToeTests
//
//  Created by Tim Leytens on 21/03/2021.
//

import XCTest
@testable import TicTacToe

class TicTacToePlayerTests: XCTestCase {

    func testModelInit() throws {

        let player1: Player = TicTacToePlayer(name: "X")
        let player2: Player = TicTacToePlayer(name: "O")

        XCTAssertEqual(player1.name, "X", "Validate player X name")
        XCTAssertEqual(player2.name, "O", "Validate player O name")
    }

}
