//
//  TicTacToeBoardTests.swift
//  TicTacToeTests
//
//  Created by Tim Leytens on 22/03/2021.
//

import XCTest
@testable import TicTacToe

class TicTacToeBoardTests: XCTestCase {

    func testWinningCombinations() throws {
        for item in TicTacToeConfiguration.Winning.allCases {
            let board: BitBoard = TicTacToeBoard()
            board.value = item.rawValue
            print(board.value)
        }
    }

    func testAllCombinations() throws {
        let max = 0b111111111
        var value: UInt = 0b000000000

        while value <= max {

            var containsWinning = false
            TicTacToeConfiguration.Winning.allCases.forEach {
                if $0.rawValue & value == $0.rawValue {
                    containsWinning = true
                }
            }

            let board: BitBoard = TicTacToeBoard()
            board.value = value

            if containsWinning {
                XCTAssertTrue(board.didWin(), "\(value) is a winning combination")
            } else {
                XCTAssertFalse(board.didWin(), "\(value) is a losing combination")
            }
            value += 1
        }

        for item in TicTacToeConfiguration.Winning.allCases {
            let board: BitBoard = TicTacToeBoard()
            board.value = item.rawValue
            XCTAssertTrue(board.didWin(), "\(item.rawValue) is a winning combination")
        }
    }

    func testMarkIndexAndReset() throws {
        let board: BitBoard = TicTacToeBoard()
        XCTAssertEqual(board.value, 0b000000000, "TicTacToe board is empty")
        board.mark(index: 0)
        XCTAssertEqual(board.value, 0b000000001, "TicTacToe marked position 0")
        board.mark(index: 1)
        XCTAssertEqual(board.value, 0b000000011, "TicTacToe marked position 1")
        board.mark(index: 2)
        XCTAssertEqual(board.value, 0b000000111, "TicTacToe marked position 2")
        XCTAssertTrue(board.didWin(), "Board is a winning combination")
        board.reset()
        XCTAssertEqual(board.value, 0b000000000, "TicTacToe board is empty")
    }
}
