//
//  TicTacToeViewController.swift
//  TicTacToe
//
//  Created by Tim Leytens on 22/03/2021.
//

import UIKit

class TicTacToeViewController: UIViewController {

    // MARK: - Private properties

    private let viewModel = TicTacToeViewModel()

    // MARK: - Outlets

    @IBOutlet private weak var collectionView: UICollectionView!

    // MARK: - View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        let nib = UINib(nibName: "TicTacToeCollectionViewCell", bundle: nil)
        collectionView.register(nib, forCellWithReuseIdentifier: "TicTacToeCollectionViewCell")
        viewModel.delegate = self
    }
}

// MARK: - UICollectionViewDelegate

extension TicTacToeViewController: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.didSelectItemAt(index: indexPath.row)
    }
}

// MARK: - UICollectionViewDataSource

extension TicTacToeViewController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        viewModel.numberOfItems
    }

    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        // swiftlint:disable force_cast
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "TicTacToeCollectionViewCell",
                                                      for: indexPath) as! TicTacToeCollectionViewCell
        cell.label.text = viewModel.valueForItemAt(index: indexPath.row)
        return cell
    }
}

// MARK: - UICollectionViewDataSource

extension TicTacToeViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let size = collectionView.frame.size.width / sqrt(CGFloat(viewModel.numberOfItems))
        return CGSize(width: size, height: size)
    }
}

// MARK: - TicTacToeViewModelDelegate

extension TicTacToeViewController: TicTacToeViewModelDelegate {

    func gameDidEnd(message: String) {
        let alert = UIAlertController(title: nil, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { [weak self] _ in
            self?.viewModel.start()
        }))
        present(alert, animated: true)
    }

    func gameDidChange() {
        collectionView.reloadData()
    }
}
