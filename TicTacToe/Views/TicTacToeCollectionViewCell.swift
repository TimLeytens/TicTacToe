//
//  TicTacToeCollectionViewCell.swift
//  TicTacToe
//
//  Created by Tim Leytens on 22/03/2021.
//

import UIKit

class TicTacToeCollectionViewCell: UICollectionViewCell {

    // MARK: - Outlets

    @IBOutlet weak var label: UILabel!

    // MARK: - Lifecycle

    override func awakeFromNib() {
        super.awakeFromNib()
        layer.borderColor = UIColor.black.cgColor
        layer.borderWidth = 1
    }
}
