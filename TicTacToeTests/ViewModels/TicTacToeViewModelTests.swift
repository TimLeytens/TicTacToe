//
//  TicTacToeViewModelTests.swift
//  TicTacToeTests
//
//  Created by Tim Leytens on 22/03/2021.
//

import XCTest
@testable import TicTacToe

class TicTacToeViewModelTests: XCTestCase {

    func testItemSelection() throws {
        let viewModel = TicTacToeViewModel()
        XCTAssertEqual(viewModel.valueForItemAt(index: 0), nil, "No player has selected the current index")

        viewModel.didSelectItemAt(index: 0)
        XCTAssertEqual(viewModel.valueForItemAt(index: 0), "X", "Player X has selected the current index")

        viewModel.start()
        XCTAssertEqual(viewModel.valueForItemAt(index: 0), nil, "No player has selected the current index")
    }
}
