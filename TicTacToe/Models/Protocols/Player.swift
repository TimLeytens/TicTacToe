//
//  Player.swift
//  TicTacToe
//
//  Created by Tim Leytens on 21/03/2021.
//

import Foundation

protocol Player {

    var name: String { get }

    var board: BitBoard { get }
}
