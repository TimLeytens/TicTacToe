//
//  TicTacToeGameServiceTests.swift
//  TicTacToeTests
//
//  Created by Tim Leytens on 21/03/2021.
//

import XCTest
@testable import TicTacToe

class TicTacToeGameServiceTests: XCTestCase {

    func testTicTacToeGameService() throws {

        let player1: Player = TicTacToePlayer(name: "X")
        let player2: Player = TicTacToePlayer(name: "O")

        let service: GameService = TicTacToeGameService(players: [player1, player2])
        service.start()

        XCTAssertEqual(service.state, .playing, "Game state is playing")
        XCTAssertEqual(service.currentPlayer?.name, player1.name, "Player X goes first")

        service.play(position: 0, completion: nil)

        XCTAssertEqual(service.currentPlayer?.name, player2.name, "Player O's turn")

        service.play(position: 1, completion: nil)

        XCTAssertEqual(service.currentPlayer?.name, player1.name, "Player X's turn")

        service.start()

        XCTAssertEqual(service.currentPlayer?.name, player1.name, "Player X goes first after restart")

        service.play(position: 1, completion: nil)

        XCTAssertEqual(service.currentPlayer?.name, player2.name, "Player O is next up")

        service.play(position: 1, completion: nil)

        XCTAssertEqual(service.currentPlayer?.name, player2.name, "Can't play invalid position")

    }

    func testTicTacToeGameServiceWinning() throws {

        let player1: Player = TicTacToePlayer(name: "X")
        let player2: Player = TicTacToePlayer(name: "O")

        let service: GameService = TicTacToeGameService(players: [player1, player2])
        service.start()
        XCTAssertEqual(service.state, .playing, "Game state playing")

        service.play(position: 0, completion: nil)
        service.play(position: 3, completion: nil)
        service.play(position: 1, completion: nil)
        service.play(position: 4, completion: nil)
        service.play(position: 2, completion: nil)
        XCTAssertEqual(service.state, .won, "Game state won")

    }

    func testTicTacToeGameServiceDraw() throws {

        let player1: Player = TicTacToePlayer(name: "X")
        let player2: Player = TicTacToePlayer(name: "O")

        let service: GameService = TicTacToeGameService(players: [player1, player2])
        service.start()
        XCTAssertEqual(service.state, .playing, "Game state playing")

        service.play(position: 0, completion: nil)
        service.play(position: 1, completion: nil)
        service.play(position: 2, completion: nil)
        service.play(position: 4, completion: nil)
        service.play(position: 3, completion: nil)
        service.play(position: 5, completion: nil)
        service.play(position: 7, completion: nil)
        service.play(position: 6, completion: nil)
        service.play(position: 8, completion: nil)
        XCTAssertEqual(service.state, .draw, "Game state draw")
    }
}
