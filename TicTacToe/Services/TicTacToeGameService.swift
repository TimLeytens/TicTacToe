//
//  TicTacToeGameService.swift
//  TicTacToe
//
//  Created by Tim Leytens on 21/03/2021.
//

import Foundation

class TicTacToeGameService: GameService {

    // MARK: - Public properties

    var state: GameState = .playing

    var players: [Player]

    var currentPlayer: Player?

    weak var delegate: GameServiceDelegate?

    // MARK: - Public methods

    public init(players: [Player]) {
        self.players = players
        reset()
    }

    func start() {
        reset()
    }

    func play(position: UInt, completion: (() -> Void)?) {

        guard let player = currentPlayer else { return }

        // Check state is playing
        guard state == .playing else { return }

        // Can't play number higher then 8
        guard position < TicTacToeConfiguration.grid else { return }

        // Can't play an already taken position
        let taken = players.reduce(0b000000000) { $0 | $1.board.value }
        guard (taken & (1 << position)) <= 0b000000000 else { return }

        player.board.mark(index: position)
        completion?()

        if player.board.didWin() {
            state = .won
            delegate?.gameDidEnd(state: state, player: player)
        } else if draw() {
            state = .draw
            delegate?.gameDidEnd(state: state, player: nil)
        } else {
            nextPlayer()
        }
    }

    // MARK: - Private methods

    private func reset() {
        state = .playing
        players.forEach { $0.board.reset() }
        currentPlayer = players.first
    }

    private func nextPlayer() {
        guard let index = (players.firstIndex { $0 .name == currentPlayer?.name }) else { return }
        currentPlayer = index + 1 < players.count ? players[index + 1] : players.first
    }

    private func draw() -> Bool {
        players.reduce(0b000000000) { $0 | $1.board.value } == 0b111111111
    }
}
