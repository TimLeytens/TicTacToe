//
//  TicTacToeViewModel.swift
//  TicTacToe
//
//  Created by Tim Leytens on 22/03/2021.
//

import Foundation

protocol TicTacToeViewModelDelegate: class {

    func gameDidEnd(message: String)
    func gameDidChange()
}

class TicTacToeViewModel: GameServiceDelegate {

    // MARK: - Public properties

    let numberOfItems = Int(TicTacToeConfiguration.grid)

    weak var delegate: TicTacToeViewModelDelegate?

    // MARK: - Private properties

    private var service: GameService

    // MARK: - Init

    init() {
        let player1 = TicTacToePlayer(name: "X")
        let player2 = TicTacToePlayer(name: "O")
        self.service = TicTacToeGameService(players: [player1, player2])
        service.delegate = self
    }

    // MARK: - Public methods

    func start() {
        service.start()
        delegate?.gameDidChange()
    }

    func didSelectItemAt(index: Int) {
        service.play(position: convertIndexPathToGameIndex(index)) { [weak self] in
            self?.delegate?.gameDidChange()
        }
    }

    func valueForItemAt(index: Int) -> String? {
        let position = convertIndexPathToGameIndex(index)
        return service.players.filter { ($0.board.value & (1 << position)) > 0 }.first?.name
    }

    // MARK: - Private methods

    private func convertIndexPathToGameIndex(_ index: Int) -> UInt {
        TicTacToeConfiguration.grid - UInt(index) - 1
    }

    // MARK: - GameServiceDelegate

    func gameDidEnd(state: GameState, player: Player?) {
        if state == .draw {
            delegate?.gameDidEnd(message: "Game ended in a draw")
        } else if state == .won, let player = player {
            delegate?.gameDidEnd(message: "🎉 \(player.name) won 🎉")
        }
    }
}
