//
//  TicTacToePlayer.swift
//  TicTacToe
//
//  Created by Tim Leytens on 21/03/2021.
//

import Foundation

struct TicTacToePlayer: Player {

    // MARK: - Public properties

    var name: String

    var board: BitBoard

    // MARK: - Init

    init(name: String) {
        self.name = name
        board = TicTacToeBoard()
    }
}
