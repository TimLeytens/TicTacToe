//
//  BitBoard.swift
//  TicTacToe
//
//  Created by Tim Leytens on 22/03/2021.
//

import Foundation

protocol BitBoard: class {

    var value: UInt { get set }

    func mark(index: UInt)

    func didWin() -> Bool

    func reset()
}
