//
//  GameService.swift
//  TicTacToe
//
//  Created by Tim Leytens on 21/03/2021.
//

import Foundation

enum GameState {
    case playing
    case won
    case draw
}

protocol GameServiceDelegate: class {
    func gameDidEnd(state: GameState, player: Player?)
}

protocol GameService {

    var state: GameState { get }

    var players: [Player] { get }

    var currentPlayer: Player? { get }

    var delegate: GameServiceDelegate? { get set }

    func start()

    func play(position: UInt, completion: (() -> Void)?)
}
