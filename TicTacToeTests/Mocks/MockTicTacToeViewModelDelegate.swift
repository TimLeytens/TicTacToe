//
//  MockTicTacToeViewModelDelegate.swift
//  TicTacToeTests
//
//  Created by Tim Leytens on 22/03/2021.
//

import Foundation
@testable import TicTacToe

class MockTicTacToeViewModelDelegate: TicTacToeViewModelDelegate {

    var gameDidEndCalled = false
    var gameDidEndMessage: String?

    var gameDidChangeCalled = false

    func gameDidEnd(message: String) {
        gameDidEndCalled = true
        gameDidEndMessage = message
    }

    func gameDidChange() {
        gameDidChangeCalled = true
    }
}
