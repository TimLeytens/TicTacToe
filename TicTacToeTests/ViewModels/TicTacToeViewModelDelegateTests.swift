//
//  TicTacToeViewModelDelegateTests.swift
//  TicTacToeTests
//
//  Created by Tim Leytens on 22/03/2021.
//

import XCTest
@testable import TicTacToe

class TicTacToeViewModelDelegateTests: XCTestCase {

    func testGameDidChangeDelegate() throws {
        let delegate = MockTicTacToeViewModelDelegate()

        let viewModel = TicTacToeViewModel()
        viewModel.delegate = delegate

        XCTAssertFalse(delegate.gameDidChangeCalled, "GameDidChange not yet called")
        viewModel.didSelectItemAt(index: 0)
        XCTAssertTrue(delegate.gameDidChangeCalled, "GameDidChange called")
    }

    func testGameDidEndDelegateWin() throws {
        let delegate = MockTicTacToeViewModelDelegate()

        let viewModel = TicTacToeViewModel()
        viewModel.delegate = delegate

        XCTAssertFalse(delegate.gameDidEndCalled, "gameDidEndCalled not yet called")
        viewModel.didSelectItemAt(index: 0)
        XCTAssertFalse(delegate.gameDidEndCalled, "gameDidEndCalled not yet called")
        viewModel.didSelectItemAt(index: 3)
        XCTAssertFalse(delegate.gameDidEndCalled, "gameDidEndCalled not yet called")
        viewModel.didSelectItemAt(index: 1)
        XCTAssertFalse(delegate.gameDidEndCalled, "gameDidEndCalled not yet called")
        viewModel.didSelectItemAt(index: 4)
        XCTAssertFalse(delegate.gameDidEndCalled, "gameDidEndCalled not yet called")
        viewModel.didSelectItemAt(index: 2)
        XCTAssertTrue(delegate.gameDidEndCalled, "gameDidEndCalled called")
        XCTAssertEqual(delegate.gameDidEndMessage, "🎉 X won 🎉")
    }

    func testGameDidEndDelegateDraw() throws {
        let delegate = MockTicTacToeViewModelDelegate()

        let viewModel = TicTacToeViewModel()
        viewModel.delegate = delegate

        XCTAssertFalse(delegate.gameDidEndCalled, "gameDidEndCalled not yet called")
        viewModel.didSelectItemAt(index: 0)
        XCTAssertFalse(delegate.gameDidEndCalled, "gameDidEndCalled not yet called")
        viewModel.didSelectItemAt(index: 2)
        XCTAssertFalse(delegate.gameDidEndCalled, "gameDidEndCalled not yet called")
        viewModel.didSelectItemAt(index: 1)
        XCTAssertFalse(delegate.gameDidEndCalled, "gameDidEndCalled not yet called")
        viewModel.didSelectItemAt(index: 3)
        XCTAssertFalse(delegate.gameDidEndCalled, "gameDidEndCalled not yet called")
        viewModel.didSelectItemAt(index: 5)
        XCTAssertFalse(delegate.gameDidEndCalled, "gameDidEndCalled not yet called")
        viewModel.didSelectItemAt(index: 4)
        XCTAssertFalse(delegate.gameDidEndCalled, "gameDidEndCalled not yet called")
        viewModel.didSelectItemAt(index: 6)
        XCTAssertFalse(delegate.gameDidEndCalled, "gameDidEndCalled not yet called")
        viewModel.didSelectItemAt(index: 7)
        XCTAssertFalse(delegate.gameDidEndCalled, "gameDidEndCalled not yet called")
        viewModel.didSelectItemAt(index: 8)
        XCTAssertTrue(delegate.gameDidEndCalled, "gameDidEndCalled called")
        XCTAssertEqual(delegate.gameDidEndMessage, "Game ended in a draw")
    }
}
