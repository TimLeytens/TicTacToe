//
//  GameServiceDelegateTests.swift
//  TicTacToeTests
//
//  Created by Tim Leytens on 22/03/2021.
//

import XCTest
@testable import TicTacToe

class GameServiceDelegateTests: XCTestCase {

    func testGameServiceDelegateWinning() throws {

        let player1: Player = TicTacToePlayer(name: "X")
        let player2: Player = TicTacToePlayer(name: "O")

        let delegate = MockGameServiceDelegate()

        var service: GameService = TicTacToeGameService(players: [player1, player2])
        service.delegate = delegate
        service.start()

        XCTAssertFalse(delegate.gameDidEndCalled, "Delegate not yet called")
        service.play(position: 0, completion: nil)
        XCTAssertFalse(delegate.gameDidEndCalled, "Delegate not yet called")
        service.play(position: 3, completion: nil)
        XCTAssertFalse(delegate.gameDidEndCalled, "Delegate not yet called")
        service.play(position: 1, completion: nil)
        XCTAssertFalse(delegate.gameDidEndCalled, "Delegate not yet called")
        service.play(position: 4, completion: nil)
        XCTAssertFalse(delegate.gameDidEndCalled, "Delegate not yet called")
        service.play(position: 2, completion: nil)
        XCTAssertTrue(delegate.gameDidEndCalled, "Delegate called")
        XCTAssertEqual(delegate.gameState, .won, "Delegate called with game state won")
    }

    func testGameServiceDelegateDraw() throws {

        let player1: Player = TicTacToePlayer(name: "X")
        let player2: Player = TicTacToePlayer(name: "O")

        let delegate = MockGameServiceDelegate()

        var service: GameService = TicTacToeGameService(players: [player1, player2])
        service.delegate = delegate
        service.start()

        XCTAssertFalse(delegate.gameDidEndCalled, "Delegate not yet called")
        service.play(position: 0, completion: nil)
        XCTAssertFalse(delegate.gameDidEndCalled, "Delegate not yet called")
        service.play(position: 1, completion: nil)
        XCTAssertFalse(delegate.gameDidEndCalled, "Delegate not yet called")
        service.play(position: 2, completion: nil)
        XCTAssertFalse(delegate.gameDidEndCalled, "Delegate not yet called")
        service.play(position: 4, completion: nil)
        XCTAssertFalse(delegate.gameDidEndCalled, "Delegate not yet called")
        service.play(position: 3, completion: nil)
        XCTAssertFalse(delegate.gameDidEndCalled, "Delegate not yet called")
        service.play(position: 5, completion: nil)
        XCTAssertFalse(delegate.gameDidEndCalled, "Delegate not yet called")
        service.play(position: 7, completion: nil)
        XCTAssertFalse(delegate.gameDidEndCalled, "Delegate not yet called")
        service.play(position: 6, completion: nil)
        XCTAssertFalse(delegate.gameDidEndCalled, "Delegate not yet called")
        service.play(position: 8, completion: nil)
        XCTAssertTrue(delegate.gameDidEndCalled, "Delegate called")
        XCTAssertEqual(delegate.gameState, .draw, "Delegate called with game state draw")
    }
}
