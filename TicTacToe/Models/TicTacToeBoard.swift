//
//  TicTacToeBoard.swift
//  TicTacToe
//
//  Created by Tim Leytens on 22/03/2021.
//

import Foundation

class TicTacToeBoard: BitBoard {

    // MARK: - Public properties

    var value: UInt = 0b000000000

    // MARK: - Public methods

    func mark(index: UInt) {
        value = value | (1 << index)
    }

    func didWin() -> Bool {
        guard value.nonzeroBitCount >= 3 else { return false }
        for item in TicTacToeConfiguration.Winning.allCases where item.rawValue & value == item.rawValue {
            return true
        }
        return false
    }

    func reset() {
        value = 0b000000000
    }
}
