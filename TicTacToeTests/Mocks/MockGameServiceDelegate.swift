//
//  MockGameServiceDelegate.swift
//  TicTacToeTests
//
//  Created by Tim Leytens on 22/03/2021.
//

import Foundation
@testable import TicTacToe

class MockGameServiceDelegate: GameServiceDelegate {

    var gameDidEndCalled = false
    var gameState = GameState.playing

    func gameDidEnd(state: GameState, player: Player?) {
        gameDidEndCalled = true
        gameState = state
    }
}
